package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.iCommandrepository;
import ru.tsc.karbainova.tm.constant.ArgumentConst;
import ru.tsc.karbainova.tm.constant.TerminalConst;
import ru.tsc.karbainova.tm.model.Command;

public class CommandRerository implements iCommandrepository {
    public static Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Exit program."
    );
    public static Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );
    public static Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.VERSION, "Display program version."
    );
    public static Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.INFO, "Display info."
    );
    public static Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, ArgumentConst.COMMANDS, "Display commands."
    );
    public static Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, ArgumentConst.ARGUMENTS, "Display arguments."
    );
    public static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            EXIT, HELP, VERSION, INFO, ABOUT, ARGUMENTS, COMMANDS
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
