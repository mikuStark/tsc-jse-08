package ru.tsc.karbainova.tm;

import ru.tsc.karbainova.tm.api.iCommandrepository;
import ru.tsc.karbainova.tm.model.Command;
import ru.tsc.karbainova.tm.repository.CommandRerository;
import ru.tsc.karbainova.tm.util.NumberUtil;

import java.util.Scanner;

import static ru.tsc.karbainova.tm.constant.TerminalConst.*;
import static ru.tsc.karbainova.tm.constant.ArgumentConst.*;


public class Application {

    private static final iCommandrepository COMMAND_REROSITORY = new CommandRerository();

    public static void main(String[] args) {
        displayWelcome();
        run(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void exit() {
        System.exit(0);
    }

    private static void parseCommand(final String param) {
        switch (param) {
            case HELP:
                displayHelp();
                break;
            case ABOUT:
                displayAbout();
                break;
            case INFO:
                showInfo();
                break;
            case VERSION:
                displayVersion();
                break;
            default:
                displayErrorCommand();
        }
    }

    private static void displayErrorCommand() {
        System.err.println("Error command!!! Command not found...");
    }

    private static void displayErrorArg() {
        System.err.println("Error argument!!! Argument not found...");
    }

    private static void parseArg(final String param) {
        switch (param) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_INFO:
                showInfo();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_COMMANDS:
                showCommands();
                break;
            case CMD_ARGUMENTS:
                showArguments();
                break;
            case CMD_EXIT:
                exit();
                break;
            default:
                displayErrorArg();
        }
    }

    private static void showInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        parseArg(param);
        System.exit(0);
    }

    private static void displayHelp() {
        Command[] commands = COMMAND_REROSITORY.getTerminalCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    public static void showCommands() {
        Command[] commands = COMMAND_REROSITORY.getTerminalCommands();
        for (final Command command : commands) {
            showCommandValue(command.getArgument());
        }
    }

    public static void showArguments() {
        Command[] commands = COMMAND_REROSITORY.getTerminalCommands();
        for (final Command command : commands) {
            showCommandValue(command.getName());
        }
    }

    private static void showCommandValue(String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("Developer: Mariya Karbainova");
        System.out.println("E-mail: mariya@karbainova");
    }
}
